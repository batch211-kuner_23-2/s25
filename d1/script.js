console.log("Hellow World");

// JSON Object
/*
-stands for Javascript Notation Object
-uses double quotes for property names
-Syntax:
{
	"propertyA": "valueA",
	"propertyB": "valueB"
}
*/


// JSON Objects
/*
"cities": [
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines" },
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines" },
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines" },
]
*/

// JSON Methods
//  the JSON object contains methods for parsing and converting data into stringified JSON

// Converting Data into stringified JSON
// stringified JSON is a Javascript object converted into a string to be used in other functions of Javascript app.

let batchesArr = [{batchName: 'BatchX'}, {batchName: 'BatchY'}]

// The "stringify" Method

console.log('Result from stringify method:');
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philppines'
	}
})
console.log(data);


//Using stringify method with variables
/*
-when information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with the variable
-this is commonly used when the information to be stored and sent to a backend application
*/ 

let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age = prompt("What is your age");
let address = {
	city: prompt("Which city do you live in?",),
	country: prompt("Which country does your city address belong to?")
};
let otherdata = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})
console.log(otherdata);

// Converting stringified JSON into Javascript objects
/*
-objects are common data types used in application because of the complex data structures that can be created out of them
-information is commonly sent to applications is stringified
*/

let batchesJSON = `[{"batchName": "BatchX"}, {"batchName": "BatchY"} ]`;
console.log('Result from parse method:');
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{"name": "John", "age": "31", "address": { "city": "Manila", "country": "Philppines"}}`

console.log(JSON.parse(stringifiedObject));